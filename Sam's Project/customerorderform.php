<!-- Author: Sam Purdy -->

<?php

	include("Header.php");
	include("navbar.php");
?>

<style>

	.card
	{
		
		margin:auto;
		font-size:25px;
	}

</style>

<?php

	include("connect_to_database.php");

	// get all of the information for each package and group it into individual variables
	$sql = "SELECT * FROM (`packages`)";

	$result = mysqli_query($dbh, $sql) or die("SQL Error");
	
	$i=0;

     foreach($result as $row)
    {
		$package_row[$i] = $row;
        ++$i;
	}

	$Caribbean = $package_row[0];
	$Polynesia = $package_row[1];
	$Asia = $package_row[2];
	$Europe = $package_row[3];
	
	print("<h2>Order Vacation Package</h2>");
	
	// check to see which package was booked by checking the data in the request and set the appropriate sql row query to the package variable
	if ($_REQUEST)
	{
		//only allow the user to book a package if they are logged in with the correct credentials. 
		if (isset($_SESSION) and $_SESSION["loggedin"] == true)
		{
			switch($_REQUEST["vac_package"])
			{

				case "Caribbean":
				$_SESSION["packageID"] = 1;
				$package = $Caribbean;
				break;

				case "Polynesia":
				$_SESSION["packageID"] = 2;
				$package = $Polynesia;
				break;

				case "Asia":
				$_SESSION["packageID"] = 3;
				$package = $Asia;
				break;

				case "Europe":
				$_SESSION["packageID"] = 4;
				$package = $Europe;
				break;

			}

			//display the selected package information on a comfirmation form on the page
			print("<form method = 'post' action='SubmitBooking.php'>");

			print("<div class='card w-75' style='width: 20rem;'>
				<div class='card-body'>
					<h4 class='card-title'>".$package['PkgName']."</h4>
					<p class='card-text'>"
					.date('Y-m-d',strtotime($package["PkgStartDate"]))." to ".
					date('Y-m-d',strtotime($package["PkgEndDate"]))."<br> 
					Cost: $");
					printf('%.2f',$package['PkgBasePrice']);
					print("<hr>");
					print("</p>");

		?>			
				<!-- allow the user to select the number of travelers in their group and the type of trip they wish to book -->
				<div class="form-group">


					<input type="number" name="TravelerCount"id="TravelerCount" placeholder="Number of travelers" class="form-control" required="required" min="1" max = "2"/>
					
					<select name="TripTypeId" class="form-control">
						<option value="">Select a Trip Type</option>

						<option value="B">Business</option>

						<option value="G">Group</option>
						
						<option value="L">Leisure</option>
						
					</select>

				</div>

					<button type = 'submit' value="send" onclick= "return confirm('Are you sure you want to book this package?')">Book Now</buttton>

			</div>
				</div>

				</form>
<?php	
		}
		else
		{
			print("You need to log in to book this package.");
		}

	}

	include("footer.php");
?>
