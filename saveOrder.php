<!--	author:Harpreet Parmar
		date:9 Nov 2017
-->


<?php
	include_once("variables.php");
	
	function validate($data)
	{
		if ($data["fname"] == "" || $data["lname"] == ""|| $data["address"] == ""|| $data["city"] == ""|| $data["province"] == ""|| $data["postalcode"] == ""|| $data["mail"]== "")
		{
			return false;
		}
		return true;
	}
	
	function insertproduct($data)
	{
		global $host, $user, $password, $dbname;
		$dbh = mysqli_connect($host, $user, $password, $dbname);
		if (!$dbh)
		{
			print(mysqli_connect_error() . "<br />");
			exit();
		}
		$sql = "INSERT INTO customers (CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, CustCountry, CustEmail, CustHomePhone, CustBusPhone, CustPassword) VALUES (?,?,?,?,?,?,'CA',?,?,?,?)";
		$stmt = mysqli_prepare($dbh, $sql);
		mysqli_stmt_bind_param($stmt, "ssssssssss", $data["fname"], $data["lname"], $data["address"], $data["city"], $data["province"], $data["postalcode"], $data["mail"], $data["homephone"], $data["busphone"], $data["password"]);
		if ($result = mysqli_stmt_execute($stmt))
		{
			//send back a success message
			$sql = "SELECT CustomerId from customers where CustFirstName=? and CustLastName=?";
			$stmt = mysqli_prepare($dbh, $sql);
			mysqli_stmt_bind_param($stmt, "ss", $data["fname"], $data["lname"]);
			mysqli_stmt_execute($stmt);
			if ($result = mysqli_stmt_get_result($stmt))
			{
				$bookingNumber = rand();
				$row = mysqli_fetch_row($result);
				$customerId = $row[0];
				$sql = "INSERT INTO bookings (BookingNo, TravelerCount, CustomerId, TripTypeId, PackageId, BookingDate) VALUES (?,?,?,?,?,".date('Y-m-d').")";
				$stmt = mysqli_prepare($dbh, $sql);
				mysqli_stmt_bind_param($stmt, "issss", $bookingNumber, $data["tavellers"], $customerId, $data["tripType"], $data["packageId"]);
				
				if ($result = mysqli_stmt_execute($stmt))
				{
					print("Booking successfull");
				}
				else
				{
					print("error");
					exit();
				}
				
			}
			else
			{
				print("error");
				exit();
			}
			
		}
		else
		{
			//send back a failure message
			print("failed");
		}
		mysqli_close($dbh);
	}
	
	if (isset($_REQUEST["fname"]))
	{
		//process data
		if (validate($_REQUEST))
		{
			insertproduct($_REQUEST);
		}
		else
		{
			header("Location: order.php");
		}
	}
	else
	{
		header("Location: order.php");
	}
?>