<!-- Author: Sam Purdy -->
<?php

    session_start();
	
	//Execute code if there is request data
    if ($_REQUEST)
	{
		//send the customer email and the password to the validate function to check for empty values.
		if (valid($_REQUEST["custemail"], $_REQUEST["password"]))
		{
            print("success");

			//send the entered username and password to the checklogin function which will check the database to see if the user entered an active, valid email and password
			if (checklogin($_REQUEST["custemail"], $_REQUEST["password"]))
			{
                print("success");
                $_SESSION["loggedin"] = true;
                $_SESSION["custemail"] = trim($_REQUEST["custemail"]);
                $_SESSION["password"] = trim($_REQUEST["password"]);
                header("Location: index.php");
				
			}
			//If any of the above conditions are not met, send the user back to the login page and display the appropriate message
			else
			{
                $_SESSION["loggedin"] = false;
                $_SESSION["message"] = "Invalid user name or password.";
                header("Location: login.php");
                print("fail");
			}
		}
		else
		{
            $_SESSION["loggedin"] = false;
            $_SESSION["message"] = "You cannot have empty fields.";
            header("Location: login.php");
            print("fail");
		}
	}
	else
	{
        $_SESSION["loggedin"] = false;
        $_SESSION["message"] = "Invalid form submit.";
        header("Location: login.php");
        print("fail");
	}
	
	//validate to see if there is data
	function valid($u, $p)
	{
		if ($u == "" or $p == "")
		{
			return false;
		}
		return true;
	}
	
	//check to see if valid data was entered
	function checklogin($u, $p)
	{
		$u = trim($u);
        $p = trim($p);

        include("connect_to_database.php");
		
		//select all of the customer ids and passwords from the database.
		$sql = "SELECT `CustEmail`, `CustPassword` FROM `customers` WHERE 1";

		$result = mysqli_query($dbh, $sql) or die("SQL Error");
		
		//read each encrypted password from the database file
		$passwords = file("hashCustPasswords.txt");
		
		print_r($passwords);
		$i = 0;

		//get each email from the database and pair it with the corresponding email in a hash table
        while($row = mysqli_fetch_row($result))
		{
			$customer[trim($row[0])] = trim($passwords[$i]);

			++$i;
		}
		 
		//check to see that the entered user email exists and that the enetered password matches with one of the encrypted passwords on file
		if (array_key_exists($u,$customer)and password_verify($p, $customer[$u]))
		{
            
            return true;
            
		}
		else
		{
            
            return false;
            
		}
	}
	
?>

