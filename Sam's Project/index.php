<!-- Author: Sam Purdy -->

<?php

    include("Header.php");
    include("navbar.php");


?> 

            <!-- create a header with the company logo and name -->
            <div class = "header-container">

                <div class = "video-container">

                    <video preload="true" autoplay = "autoplay" loop="loop" volume = "0" >
                
                        <source src = "Airplane.mp4" type ="video/mp4">

                    </video><!--end of video -->

                    <img style="vertical-align: middle" src="logo.jpeg" id="logo" height="100" width="100">

                    <h1 id ="header">TravelExperts.ca</h1>

                </div> <!--end of video container -->

            </div> <!--end of header container -->

            <!-- place the content of the page in a bootstrap container for dynamic display purposes -->
            <div class="container-fluid" >

                <section>                   

                    <h2>Welcome to Travel Experts! We have plenty of great travel packages for popular travel locations. Register Today!</h2>

                    <!-- All open-source images from http://www.pixels.com --> 
                    
                        <!-- display a bootstrap slideshow with random travel images -->
                        <div id="carouseldiv" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner" role="listbox">

                                    <script>
                                    
                                    for(i=0;i<10;i++)
                                    {
                                    if(i==0)
                                    {
                                        document.write("<div class='carousel-item active'><img class='d-block img-fluid' src='Images/img"+(i+1)+".jpeg' alt='First Slide'></div>");
                                    }else
                                    {
                                        document.write("<div class='carousel-item'><img class='d-block img-fluid' src='images/img"+(i+1)+".jpeg' alt='No slides here'></div>");
                                    }
                                    }
                                    
                                    </script>   


                                </div>

                            </div> 

                            <div class="row">
                                <div class="col">
                                    <h2>We have a different Featured Vacation Package every week! Book now and save!</h2>
                                </div>
                            </div>
                            
                            <!-- code that will display a featured vacation of the week by accessing the database and displaying travel package information neatly in a bootstrap card -->
                            <?php

                                    include("GetFeaturedVacation.php");
                            ?>
                            
                            <div class="card mb-3">
                                <img id = "PackageImage" class="card-img-top" src="" alt="Card image cap">
                                <script>getVacationImage();</script>
                                <div class="card-block">
                                    <h4 class="card-title">Featured Package: <?php print($featured["PkgName"])?></h4>
                                    <p class="card-text">
                                    <?php print($featured["PkgDesc"]) ?>
                                    </p>
                                    <p class="card-text"><small class="text-muted">Base Price: $<?php printf("%.2f", $featured["PkgBasePrice"]) ?></small></p>
                        
                                    <a href="register.php"><button class="btn btn-dark">Click Here to Book This Package!</button></a>
                
                                    <a href="Vacation.php"><button class="btn btn-dark">Click Here to See All Packages</button></a>
                                    
                                </div>
                            </div>
                       
                </section>


  <?php

include("footer.php");

  ?>        
    
            <script>

               // If the window is full screen, animate the header title to move across the screen. Otherwise, keep the header title static to prevent layout problems on the page.                    
                if(window.innerWidth>1300){
        
                    animate();
        
                    }else{
        
                    var header_title = document.getElementById("header");
        
                    header_title.style.position = "static";

                    }
                
                </script>

            