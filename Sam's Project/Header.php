
<!DOCTYPE html>
<html>

    <head>

         <meta charset="utf-8" />
         
         <meta name="viewport" content="width=device-width, initial-scale=1">

         <title>Travel Experts</title>

         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

         <link rel="stylesheet" type="text/css" href="https://meyerweb.com/eric/tools/css/reset/reset.css">

         <link rel="stylesheet" type="text/css" href="styles.css">

         <link href="https://fonts.googleapis.com/css?family=Norican" rel="stylesheet">
         <link href="https://fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">

         <script src="script.js"></script>

    </head>

        <body>


