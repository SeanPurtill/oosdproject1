<!-- Author: Sam Purdy -->

<style>

.navbar
{

    font-size: 20px;
    
}    

    
</style>

<!-- Sam Purdy
November 5, 2017 -->

<!-- bootstrap navbar to be used in each html page -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <a class="navbar-brand" href="index.php">Travel Experts</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">

    <ul class="navbar-nav mr-auto">

      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current) </span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="contact.php">Contact </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="Vacation.php">Vacation Packages</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link sign_in" href="register.php">Register </a>
      </li>

      <li class="nav-item">
        <a class="nav-link sign_in" href="login.php">Login </a>
      </li>

    </ul>

  </div>

</nav>

<script type="text/javascript">

    getActivePage();

</script>

<?php
 session_start();

    if (isset($_SESSION["loggedin"]) and $_SESSION["loggedin"] == true)
    {
       print("<a id='profilelink' style = 'position:absolute; top:20px; right:85px;' href = 'login.php'>User ".$_SESSION["custemail"]." Logged In </a>");
    }
?>