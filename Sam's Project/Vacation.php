<!-- Author: Andrew -->
<?php
include("Header.php");
include("navbar.php");

?>

<!-- available vacation packages -->
<form action="customerorderform.php" method = "post">


                    <?php

                         include("GetPackage.php");
                         
						$currDate = date('Y-m-d H:i:s');
						if($currDate < $Caribbean["PkgEndDate"])
						{
				   ?>
                            <div class="card mb-3">
                                <img id = "PackageImage" class="card-img-top" src="PackageImages/Caribbean.jpg" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title"> Package: <?php print($Caribbean["PkgName"])?></h4>
                                    <p class="card-text">
                                    <?php print($Caribbean["PkgDesc"]) ?>
                                    </p>
                                    <p <?php $currDate = date('Y-m-d H:i:s'); if($currDate > $Caribbean["PkgStartDate"]) {?> style="color:red" <?php }?>>
                                    Start Date: <?php print(date('Y-m-d',strtotime($Caribbean["PkgStartDate"]))) ?>
									</p>
                                    <p>End Date: <?php print(date('Y-m-d',strtotime($Caribbean["PkgEndDate"]))) ?>
                                    </p>
                                    <p class="card-text"><small class="text-muted">Base Price: $<?php printf("%.2f", $Caribbean["PkgBasePrice"]) ?></small></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <button type = "submit" name="vac_package" value = "Caribbean" class="btn btn-dark">Click Here to Book This Package!</button></br>
                                </div>

                               

                            </div>
						<?php
							}
						?>
						<?php
							$currDate = date('Y-m-d H:i:s');
							if($currDate < $Polynesia["PkgEndDate"])
							{
						?>
                            <div class="card mb-3">
                                <img id = "PackageImage" class="card-img-top" src="PackageImages/Polynesia.jpeg" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title"> Package: <?php print($Polynesia["PkgName"])?></h4>
                                    <p class="card-text">
                                    <?php print($Polynesia["PkgDesc"]) ?>
                                    </p>
                                    <p <?php $currDate = date('Y-m-d H:i:s'); if($currDate > $Polynesia["PkgStartDate"]) {?> style="color:red" <?php } ?>>
                                    Start Date: <?php print(date('Y-m-d',strtotime($Polynesia["PkgStartDate"]))) ?> </p>
                                    <p>End Date: <?php print(date('Y-m-d',strtotime($Polynesia["PkgEndDate"]))) ?>
                                    </p>
                                    <p class="card-text"><small class="text-muted">Base Price: $<?php printf("%.2f", $Polynesia["PkgBasePrice"]) ?></small></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <button type = "submit" name="vac_package" value = "Caribbean" class="btn btn-dark">Click Here to Book This Package!</button></br>
                                </div>

                               

                            </div>
						<?php
							}
						?>
						
						<?php
							$currDate = date('Y-m-d H:i:s');
							if($currDate < $Asia["PkgEndDate"])
							{
						?>
                            <div class="card mb-3">
                                <img id = "PackageImage" class="card-img-top" src="PackageImages/Asia.jpeg" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title"> Package: <?php print($Asia["PkgName"])?></h4>
                                    <p class="card-text">
                                    <?php print($Asia["PkgDesc"]) ?>
                                    </p>
                                    <p <?php $currDate = date('Y-m-d H:i:s'); if($currDate > $Asia["PkgStartDate"]) {?> style="color:red" <?php } ?>>
                                    Start Date: <?php print(date('Y-m-d',strtotime($Asia["PkgStartDate"]))) ?> </p>
                                    <p>End Date: <?php print(date('Y-m-d',strtotime($Asia["PkgEndDate"]))) ?>
                                    </p>
                                    <p class="card-text"><small class="text-muted">Base Price: $<?php printf("%.2f", $Asia["PkgBasePrice"]) ?></small></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <button type = "submit" name="vac_package" value = "Caribbean" class="btn btn-dark">Click Here to Book This Package!</button></br>
                                </div>

                               

                            </div>
						<?php
							}
						?>
						
						<?php
							$currDate = date('Y-m-d H:i:s');
							if($currDate < $Europe["PkgEndDate"])
							{
						
						
						?>
                            <div class="card mb-3">
                                <img id = "PackageImage" class="card-img-top" src="PackageImages/Europe.jpeg" alt="Card image cap">
                                <div class="card-block">
                                    <h4 class="card-title"> Package: <?php print($Europe["PkgName"])?></h4>
                                    <p class="card-text">
                                    <?php print($Europe["PkgDesc"]) ?>
                                    </p>
                                    <p <?php $currDate = date('Y-m-d H:i:s'); if($currDate > $Europe["PkgStartDate"]) {?> style="color:red" <?php } ?>>
                                    Start Date: <?php print(date('Y-m-d',strtotime($Europe["PkgStartDate"]))) ?> </p>
                                    <p>End Date: <?php print(date('Y-m-d',strtotime($Europe["PkgEndDate"]))) ?>
                                    </p>
                                    <p class="card-text"><small class="text-muted">Base Price: $<?php printf("%.2f", $Europe["PkgBasePrice"]) ?></small></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col ">
                                    <button type = "submit" name="vac_package" value = "Caribbean" class="btn btn-dark">Click Here to Book This Package!</button></br>
                                </div>

                               

                            <?php
							    }
						    ?>


</form>

</div>
<?php

include("footer.php");

?>        

