<!-- Author: Sam Purdy -->

<!-- access the database and retrieve a vacation package based on what day of the month it is. Display information about the current package on the home page -->
<?php
                              
    include("connect_to_database.php");

    $sql = "SELECT PkgName, PkgDesc, PkgBasePrice FROM (`packages`)";

    $result = mysqli_query($dbh, $sql) or die("SQL Error");

    $i=0;
    foreach($result as $row)
    {
        $package[$i] = $row;
        ++$i;
    }


    $day = date("d");

    switch($day)
    {
        case ($day>=1 && $day<=7):
        $featured = $package[0];
        break;

        case ($day>7 && $day<=15):
        $featured = $package[1];
        break;

        case ($day>15 && $day<=22):
        $featured = $package[2];
        break;

        case ($day>22 && $day<=31):
        $featured = $package[3];
        break;

    }
    
?>