<!-- Author: Sam Purdy -->

<!-- connect to the travelexperts database -->
<?php

include_once("variables.php");

$dbh = mysqli_connect($host, $user, $password, $dbname);

if (!$dbh)
{
    print(mysqli_connect_errno() . "<br />");
    print("Error Message: ".mysqli_connect_error()."<br/>");
    exit();
}

?>