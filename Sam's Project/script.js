
  // get the vacation images from the folder and display the corresponding image based on the week of the month
  // Author: Sam
  function getVacationImage()
  {
  var Package_Image = [];

  Package_Image[0] = "PackageImages/Caribbean.jpg"; 
  Package_Image[1] = "PackageImages/Polynesia.jpeg"; 
  Package_Image[2] = "PackageImages/Asia.jpeg";
  Package_Image[3] = "PackageImages/Europe.jpeg"; 

  var d = new Date();
  var day = d.getDate();

 var i;
 if(day>=1 && day<=7)
 {
    i=0;
 }
 if(day>7 && day<=15)
 {
    i=1;
 }
 else if(day>15 && day<22)
 {
    i=2;
 }      
 else if(day>22 && day<=31)
 {
    i=3;
 } 
    var img = document.getElementById("PackageImage"); 
    img.src = Package_Image[i];
  
  }

// code to animate the header title, using its absolute position
// Author: Sam
function animate(){

    var heading = document.getElementById("header");


    var id = setInterval(frame,5);
    var position = 0;
   
    function frame(){

        if(position == 800)
        {
            clearInterval(id);
            heading.style.position = "static";
        }else{
            position++;
            heading.style.right = position + 'px';
            
        }
    }

}

//get the active page based on the url and set the appropriate style for the navbar
// Author: Sam
// get the active page based on the url and set the navbar link to active class
function getActivePage()
{
    var path = window.location.pathname;
    var page = path.split("/").pop();

    var page_links = document.getElementsByTagName("li");

    for(i=0;i<page_links.length;i++)
    {

        page_links[i].classList.remove("active");

    }

    switch (page) {
      case "index.php":
        page_links[0].classList.add("active");
        break;

      case "contact.php":
        page_links[1].classList.add("active");
        break;

      case "Vacation.php":
        page_links[2].classList.add("active");
        break;

      case "register.php":
        page_links[3].classList.add("active");
        break;

      case "login.php":
        page_links[4].classList.add("active");
        break;

      default:
        page_links[0].classList.add("active");
    }

}

// form validation for the customer registration page
// Author: Harpreet
function submitAction(myform) {
  if (myform.fname.value == "") {
    alert("Firstname is empty");
    myform.fname.focus();
    return false;
  }
  if (myform.lname.value == "") {
    alert("Lastname is empty");
    myform.lname.focus();
    return false;
  }
  if (myform.address.value == "") {
    alert("Address is empty");
    myform.address.focus();
    return false;
  }
  if (myform.city.value == "") {
    alert("City is empty");
    myform.city.focus();
    return false;
  }
  if (myform.province.value == "") {
    alert("Province is empty");
    myform.province.focus();
    return false;
  }
  if (myform.postelcode.value == "") {
    alert("Postel code is empty");
    myform.postelcode.focus();
    return false;
  }
  if (myform.agentId.value == "") {
    alert("Agent id is empty");
    myform.agentId.focus();
    return false;
  }
  if (myform.password.value == "") {
    alert("password is empty");
    myform.password.focus();
    return false;
  }
  if (myform.businessphonenumber.value == "") {
    alert(" Business phone number is empty");
    myform.businessphonenumber.focus();
    return false;
  }
  if (myform.homephonenumber.value == "") {
    alert("Home phone number  is empty");
    myform.homephonenumber.focus();
    return false;
  }
  return confirm("Are you sure you want to submit this form?");
}





