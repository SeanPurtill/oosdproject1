/*	author:Harpreet Parmar
		date:9 Nov 2017
*/

function submitAction(myform)
{
	if (myform.fname.value == "")
	{
		alert("Firstname is empty");
		myform.fname.focus();
		return false;
	}
	if (myform.lname.value == "")
	{
		alert("Lastname is empty");
		myform.lname.focus();
		return false;
	}
	if(myform.address.value=="")
	{
		alert("Address is empty");
		myform.address.focus();
		return false;
	}
	if(myform.city.value=="")
	{
		alert("City is empty")
		myform.city.focus();
		return false;
	}
	if(myform.province.value=="")
	{
		alert("Province is empty")
		myform.province.focus();
		return false;
	}
	if(myform.postelcode.value=="")
	{
		alert("Postel code is empty")
		myform.postelcode.focus();
		return false;
	}
	 if(myform.agentId.value=="")
	{
		alert("Agent id is empty");
		myform.agentId.focus();
		return false;
	}
	if (myform.password.value == "")
	{
		alert("password is empty");
		myform.password.focus();
		return false;
	}
   if(myform.businessphonenumber.value=="")
	{
		alert(" Business phone number is empty");
		myform.businessphonenumber.focus();
		return false;
	}
	if(myform.homephonenumber.value=="")
	{
		alert("Home phone number  is empty");
		myform.homephonenumber.focus();
		return false;
	}
	return confirm("Are you sure you want to submit this form?");
}