<!-- Author: Sam Purdy -->

<?php

    include("Header.php");
    include("navbar.php");
    
    if(!isset($_SESSION['loggedin']) ){
        $_SESSION['loggedin'] = false;    
    }

?>

<div class= "Login_Content">

<form autocomplete = "off" method = "post" action="verify_customer_login.php" class="form-group">

<div class="form-group">

    <div class="col">
       <span class="labels">Email: </span><input autocomplete = "off" type = "text" class = "form-control" name = "custemail" value="">
    </div><br>

    <div class="col">
        <span class="labels">Password: </span><input autocomplete = "off" type = "password" class = "form-control" name = "password" value="">
    </div>

</div> 
<?php

//check the loggin state and change the value of the login button accordingly
 if($_SESSION["loggedin"]==true)
    {
        $loginstate = "Log Out";
    }else
    {
        $loginstate = "Log In";
    }
?>

    <input id = "loginbutton" type="submit" value="<?php print($loginstate);?>" class="btn btn-primary">
    
</form>
<p><?php 
    

    if (isset($_SESSION["message"]))
    {
        print($_SESSION["message"]);
        unset($_SESSION["message"]);
    }

?></p>
<?php
    if($_SESSION["loggedin"] == false)
    {
        print("User Logged Out.");
    }else{
        print("User Logged In.");
    }

    include("footer.php");
?>
