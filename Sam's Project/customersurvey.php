<!-- Author: Sam Purdy -->

<?php

    include("Header.php");
    include("navbar.php");
?>
<style>

    .radio
    {
        margin-bottom: 20px;
    }
    label{
        margin-bottom: 10px;
    }

</style>


            <!-- create a div for separating the page content from the navbar using the top margin attribute -->
            <div class= "content">

            <!-- create a header with the company logo and name -->
            <div class = "header-container">

                <div class = "video-container">

                    <video preload="true" autoplay = "autoplay" loop="loop" volume = "0" >

                
                        <source src = "Airplane.mp4" type ="video/mp4">

                    </video><!--end of video -->

                    <img style="vertical-align: middle" src="logo.jpeg" id="logo" height="100" width="100">

                    <h1 id ="header">TravelExperts.ca</h1>

                </div> <!--end of video container -->

            </div> <!--end of header container -->

            <!-- place the content of the page in a bootstrap container for dynamic display purposes -->
            <div class="container-fluid" >

                <section>                   




<header >
        <h2>Customer Survey</h2>
</header>

            <section class = "container">

                <!-- set up a form withe customer survey questions and post the results to the travelexperts database -->
                <form method="post" action="" id = "Agent_Form" class="form-horizontal">

                <div class="radio">

                     <label for="TravelPerYear"><h3>How many times per year do you travel?</h3> </label><br>

                    <input type="radio" name="TravelPerYear" value="1to2" checked> 1 to 2<br>
                    <input type="radio" name="TravelPerYear" value="3to4"> 3 to 4<br>
                    <input type="radio" name="TravelPerYear" value="morethan4"> More than 4 <br>
                    <input type="radio" name="TravelPerYear" value="notravel"> I don't travel at all

                </div>

                <div class="radio">

                     <label for="TravelAccomodations"><h3>Where do you usually stay when you travel?</h3> </label><br>

                    <input type="radio" name="TravelAccomodations" value="hotels" checked> hotels<br>
                    <input type="radio" name="TravelAccomodations" value="hostels"> hostels<br>
                    <input type="radio" name="TravelAccomodations" value="family"> With family or friends<br>
                    <input type="radio" name="TravelAccomodations" value="camping"> Camping/tents <br>
                    <input type="radio" name="TravelAccomodations" value="other"> Other

                </div>

                <div class="radio">

                     <label for="TravelCost"><h3>About how much would you say you spend on an average family vaction?</h3> </label><br>

                    <input type="radio" name="TravelCost" value="less1000" checked> less than $1000<br>
                    <input type="radio" name="TravelCost" value="1000to3000"> between $1000 and $3000<br>
                    <input type="radio" name="TravelCost" value="3000to5000"> between $3000 and $5000<br>
                    <input type="radio" name="TravelCost" value="5000to8000"> between $5000 and $8000 <br>
                    <input type="radio" name="TravelCost" value="Morethan8000"> More than $8000

                </div>

                <div class="form-group">
                <label for="sel1"><h3>Select the location you would most likely travel to on a vacation. </h3></label>
                    <select name = "TravelLocation" class="form-control" id="sel1">
                        <option value = "US">United States</option>
                        <option value = "EUR">Europe</option>
                        <option value = "AFR">Africa</option>
                        <option value = "ASIA">Asia</option>
                        <option value = "AUS">Australia</option>
                        <option value = "SA">South America</option>
                    </select>
                </div>

                <div class="form-group">

                            <label for="TravelGroupSize"><h3>How Many people do you usually travel with:</h3> </label>
                            <input type="number" name="TravelGroupSize" id="TravelGroupSize" placeholder="2" class="form-control" required="required" min="0" max = "10"/></br>

                </div>
                          
                <div class="form-group">

                <button type="submit" value="Send" class="btn btn-default">Submit</button>
                <button type="reset" class="btn btn-default">Reset</button>

                </div>

                </form>

                </section>
<?php
               
    
    //connect to the database and insert all form data as a row into the customer survey data table.
    include("connect_to_database.php");

    if($_REQUEST)
    {
        $data = $_REQUEST;

        $keys = implode(",",array_keys($data));
        
        $values  = array_values($data);
   
        $sql = "INSERT INTO `customer_survey_data`($keys) VALUES ('$values[0]','$values[1]','$values[2]','$values[3]','$values[4]')";

        $result = mysqli_query($dbh, $sql);

        if ($result)
        {
        print("Thank you for your participation.");
        }
        else
        {
        print("Error Submitting form.");
        } 
                

    }

?>

</body>
    </html>
