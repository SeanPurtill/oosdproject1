<!-- Author: Sean Purtill-->
<?php
include("Header.php");
include("navbar.php");

?>

<script>

    
//    function to make calgary staff table appear and okatoks dissappear
    function showcal()
    {
        document.getElementById("table2").style.display = "table";    
        document.getElementById("table3").style.display = "none";
        
//    function to make okatoks staff table appear    
    }
    function showoka()
    {
        document.getElementById("table3").style.display = "table";    
        document.getElementById("table2").style.display = "none";    
    }

</script>

<br/>
<h2>Come visit us at the following locations</h2>    
  
   <?php     
      include("connect_to_database.php");
    
//creates table and inputs data from the agency table    
if ($result = @mysqli_query($dbh, "SELECT `AgncyCity`,`AgncyAddress`,`AgncyPhone` FROM `agencies`"))
            {
                print("<table id='table1'>");
                
                echo '<tr><th><u>City</u></th><th><u>Address</u></th><th><u>Phone</th></u></tr>';
                $first = true;
                while ($row = mysqli_fetch_row($result))
                {
                    if ($first)
                        {
                        print("</tr>");
                        $first=false;
                        reset($row);
                        }
                        print("<tr>");
                        foreach ($row as $k=>$v)
                            {
                                print("<td>$v</td>");
                            }
                    print("</tr>");
                }
                print("</table>");
            }    

?>

<h2> Click on the storefronts below to get in contact with specific staff from our locations</h2>
   
<!--displays the images in a table for the storefronts-->
<table id="storefronttable">
    <tr>
        <td><img id="storefront" src="Images/calgary.jpg" onclick="showcal()"/></td>
        <td><img id="storefront" src="Images/Okatoks.jpg" onclick="showoka()"/></td>

    </tr>

    
</table>



<?php    
    //creates table and inputs data from the agents table, but only by agents from Calgary    

       if ($result = @mysqli_query($dbh, "SELECT `AgtFirstName`,`AgtLastName`,`AgtBusPhone`,`AgtEmail`,`AgtPosition` FROM `agents` WHERE `AgencyId` = 1"))
            {
                                print("<br/>");
                                print("<table id='table2'>");
           echo '<th colspan="5">Calgary</th>';
           echo '<tr><th colspan="2">Name</th><th>Phone</th><th>Email</th><th>Position</th></tr>';
                $first = true;
                while ($row = mysqli_fetch_row($result))
                {
                    if ($first)
                        {
                        print("</tr>");
                        $first=false;
                        reset($row);
                        }
                        print("<tr>");
                        foreach ($row as $k=>$v)
                            {
                                print("<td>$v</td>");
                            }
                    print("</tr>");
                }
                print("</table>");
            }

    //creates table and inputs data from the agents table, but only by agents from Okatoks
    if ($result = @mysqli_query($dbh, "SELECT `AgtFirstName`,`AgtLastName`,`AgtBusPhone`,`AgtEmail`,`AgtPosition` FROM `agents` WHERE `AgencyId` = 2"))
            {
   
          print("<table id='table3'>");
                echo '<th colspan="5">Okatoks</th>';
                echo '<tr><th colspan="2">Name</th><th>Phone</th><th>Email</th><th>Position</th></tr>';
                $first = true;
                while ($row = mysqli_fetch_row($result))
                {
                    if ($first)
                        {
                        print("</tr>");
                        $first=false;
                        reset($row);
                        }
                        print("<tr>");
                        foreach ($row as $k=>$v)
                            {
                                print("<td>$v</td>");
                            }
                    print("</tr>");
                }
                print("</table>");
            }
        else
            {
                print("error number: " . mysqli_errno($dbh) . "<br/>");
                print("error: " . mysqli_error($dbh) . "<br/>");
            }
    
    mysqli_close($dbh);
        
    ?>  
    
    </h2>
<?php

include("footer.php");

?>        
    </body>
</html>

