<!--	author:Harpreet Parmar
		date:9 Nov 2017
-->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="./styles.css" />
  <script src="registerScript.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.html">Travel Experts</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="index.html">Home</a></li>
      <li><a href="contact.html">Agent information</a></li>
      <li class="active"><a href="registration.html">Registration</a></li>
    </ul>
  </div>
</nav>
<div class="container">
<center>
    <h1><font color="#454140">Customer Registration</font></h1>
  </center>
  <div class="row">
  <form method="post" action="saveOrder.php">
    <div id=divOverall>
    <div id=wrapper>
      <div id=multipleInsideWrapper>
      <label>First name:</label><br>
        <input type="text" name ="fname" id ="fname" placeholder="First Name" required="required" />
        </div>
      <div id=multipleInsideWrapper>
      <label>Last name:</label><br>
        <input type= "text" name="lname" id="lname" placeholder="Last Name" required="required"/>
        </div>
    </div>

    <div id=wrapper>
      <div id= multipleInsideWrapper
        <label>Address:</label>
        <input type="text" name="address" id="address" placeholder="Address" />
      </div>
      <div id = multipleInsideWrapper>
        <label>City:</label>
        <input type="text" name="city" id="city" placeholder="City" />
      </div>
    </div>

    <div id=wrapper>
      <div id= multipleInsideWrapper>
        <label>Province:</label>
        <select name="province">
          <option value="ab"> Alberta </option>
          <option value="bc"> British Columbia</option>
          <option value="sk"> Saskatchchewan </option>
          <option value="mb"> Manitoba </option>
          <option value="on"> Ontario </option>
          <option value="qc"> Quebec </option>
          <option value="nc"> Nova scotia </option>
          <option value="nl"> Newfoundland and labrador </option>
          <option value="nb"> New Burnswick </option>
          <option value="pei"> Prince Edward Island </option>
          <option value="nu"> Nunavut </option>
          <option value="nt"> Northweat Territories </option>
          <option value="yt"> Yukon </option>
        </select>
      </div>
      <div id = multipleInsideWrapper>
        <label>Postal Code:</label>
        <input type="text" name="postalcode" id ="postalcode" placeholder="Postal Code" pattern="[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]" title="Enter Valid Postal Code eg. A1A 1B1"/>
      </div>
    </div>

    <div id=wrapper>
      <div id = multipleInsideWrapper>
        <label>Country:</label>
        <input type="text" name="country" id = "counry" placeholder="Country" />
      </div>
		  <div id = multipleInsideWrapper>
			<label>Package Id: </label>
			<input type="text" name="packageId" id ="packageId" placeholder="packageId" />
		  </div>
     </div>
	 
     <div id=wrapper>
      <div id = multipleInsideWrapper>
        <label>AgentId:</label>
        <input type="text" name="agentid" id = "agentid" placeholder="AgentId" />
      </div>
	    <div id=multipleInsideWrapper>
      <label>Password:</label><br>
        <input type= "password" name="password" id="password" placeholder="Password" />
        </div>
		</div>
		
		
     <div id=wrapper>
      <div id = multipleInsideWrapper>
        <label>Business Phone Number:</label>
        <input type="text" name="busphone" id = "busphone" placeholder="Business Phone " />
      </div>
	    <div id=multipleInsideWrapper>
      <label>Home Phone Number:</label><br>
        <input type= "text" name="homephone" id="homephone" placeholder="Home Phone" />
        </div>
		</div>
    
    <div id=wrapper>
      <div id = multipleInsideWrapper>
        <label>Visa:</label>
        <input type="radio" name="cardtype" id ="visa" value="v" checked="checked"/>
        <label>MasterCard:</label>
        <input type="radio" name="cardtype" id="mcard" value="m" checked="checked"/>
        <label>American:</label>
        <input type="radio" name="cardtype" id="amex" checked="checked"/>
      </div>
      <div id = multipleInsideWrapper>
        <label>Send to me in the mailbox:</label>
        <input type="checkbox" name="sendmail" id ="sendmail" value="yes" checked="checked"/>
      </div>

    </div>
    <br>
    
    <div id=wrapper>
      <div id= multipleInsideWrapper>
        <label>The number of people in the group:</label><br>
        <input type="number" name ="tavellers" id="travellers" min="1" max="8"/>
      </div>
      <div id = multipleInsideWrapper>
        <label>Reason for trip</label><br>
        <select name="tripType">
          <option value="B"> Business </option>
          <option value="G"> Group</option>
          <option value="L"> Leisure </option>
        </select>
      </div>
    </div>
    
    <div id=wrapper>
      <div id= multipleInsideWrapper>
        <label>Email:</label><br>
        <input type="mail" name="mail" placeholder="Email" />
      </div>
      <div id = multipleInsideWrapper>
        <label>Tell us about yourself:</label>
        <textarea name="about" required="required" placeholder="About you"></textarea>
      </div>
    </div>
    </div>
    
    <div id=wrapper>
      <div id= multipleInsideWrapper>
        <input type="submit" onclick= "return(submitAction(this.form))"/>
      </div>
      <div id = multipleInsideWrapper>
        <input type="reset" onclick = "return confirm('Do you really want to reset?');"/>
      </div>
    </div>
  </form>
  </div>
</div>

</body>
</html>
